--[[

	Holograms / Floating text Script by Meh
	
	Just put in the coordinates you get when standing on the ground, it's above the ground then
	By default, you only see the hologram when you are within 10m of it, to change that, edit the 10.0 infront of the "then"
	The Default holograms are at the Observatory.
	
	If you want to add a line to the hologram, just make a new Draw3DText line with the same coordinates, and edit the vertical offset.
	
	Formatting:
			Draw3DText( x, y, z  vertical offset, "text", font, text size, text size)
			
			
	To add a new hologram, copy paste this example under the existing ones, and put coordinates and text into it.
	
		if GetDistanceBetweenCoords( X, Y, Z, GetEntityCoords(GetPlayerPed(-1))) < 10.0 then
			Draw3DText( X, Y, Z,  -1.400, "TEXT", 4, 0.1, 0.1)
			Draw3DText( X, Y, Z,  -1.600, "TEXT", 4, 0.1, 0.1)
			Draw3DText( X, Y, Z,  -1.800, "TEXT", 4, 0.1, 0.1)		
		end


]]--

Citizen.CreateThread(function()
    Holograms()
end)

function Holograms()
		while true do
			Citizen.Wait(0)			
				--Mission Row 2nd floor office
		if GetDistanceBetweenCoords( 462.83, -1000.35, 35.93, GetEntityCoords(GetPlayerPed(-1))) < 3.0 then
			Draw3DText( 462.83, -1000.35, 35.93  -1.400, "Commissioner Pickles", 4, 0.1, 0.1)
			Draw3DText( 462.83, -1000.35, 35.93  -1.600, "Deputy Commissioner Fulton", 4, 0.1, 0.1)
			Draw3DText( 462.83, -1000.35, 35.93  -1.800, "", 4, 0.1, 0.1)		
		end

				--Mission Row office
				if GetDistanceBetweenCoords( 447.05, -980.61, 30.69, GetEntityCoords(GetPlayerPed(-1))) < 3.0 then
					Draw3DText( 447.05, -980.61, 30.69  -1.400, "", 4, 0.1, 0.1)
					Draw3DText( 447.05, -980.61, 30.69  -1.600, "Chief of Police", 4, 0.1, 0.1)
					Draw3DText( 447.05, -980.61, 30.69  -1.800, "", 4, 0.1, 0.1)		
				end		
			--Mission Row Shop
		if GetDistanceBetweenCoords( 468.05, -990.19, 30.69, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
			Draw3DText( 468.05, -990.19, 30.69  -1.400, "Police", 4, 0.1, 0.1)
			Draw3DText( 468.05, -990.19, 30.69  -1.600, "Shop", 4, 0.1, 0.1)
			Draw3DText( 468.05, -990.19, 30.69 -1.800, "", 4, 0.1, 0.1)		
		end
	
			--Sandy Shop
			if GetDistanceBetweenCoords( 1845.26, 3694.08, 34.26, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 1845.26, 3694.08, 34.26  -1.400, "Police", 4, 0.1, 0.1)
				Draw3DText( 1845.26, 3694.08, 34.26  -1.600, "Shop", 4, 0.1, 0.1)
				Draw3DText( 1845.26, 3694.08, 34.26  -1.800, "", 4, 0.1, 0.1)		
			end
			
			--Paleto Shop
			if GetDistanceBetweenCoords( -443.67, 6004.22, 31.72, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( -443.67, 6004.22, 31.72  -1.400, "Police", 4, 0.1, 0.1)
				Draw3DText( -443.67, 6004.22, 31.72  -1.600, "Shop", 4, 0.1, 0.1)
				Draw3DText( -443.67, 6004.22, 31.72  -1.800, "", 4, 0.1, 0.1)		
			end

			--Job Office
		if GetDistanceBetweenCoords( 239.48, -417.41, -115.11, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
			Draw3DText( 239.48, -417.41, -115.11  -1.400, "Commercial City ", 4, 0.1, 0.1)
			Draw3DText( 239.48, -417.41, -115.11  -1.600, "Employment Office", 4, 0.1, 0.1)
			Draw3DText( 239.48, -417.41, -115.11  -1.800, "", 4, 0.1, 0.1)		
		end	

			--Classroom
			if GetDistanceBetweenCoords( 469.4, -1010.21, 26.39, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 469.4, -1010.21, 26.39  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 469.4, -1010.21, 26.39  -1.600, "Classroom", 4, 0.1, 0.1)
				Draw3DText( 469.4, -1010.21, 26.39  -1.800, "", 4, 0.1, 0.1)		
			end			

			--Dispatch
			if GetDistanceBetweenCoords( 445.04, -989.27, 35.93, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 445.04, -989.27, 35.93  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 445.04, -989.27, 35.93  -1.600, "Dispatch Center", 4, 0.1, 0.1)
				Draw3DText( 445.04, -989.27, 35.93  -1.800, "", 4, 0.1, 0.1)		
			end				

			--Medic Time Clock
			if GetDistanceBetweenCoords( 287.57, -1452.62, 29.97, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 287.57, -1452.62, 29.97  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 287.57, -1452.62, 29.97  -1.600, "Time Clock", 4, 0.1, 0.1)
				Draw3DText( 287.57, -1452.62, 29.97  -1.800, "", 4, 0.1, 0.1)		
			end	
			
			--Medical Directors Office
			if GetDistanceBetweenCoords( 315.9, -1467.53, 37.91, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 315.9, -1467.53, 37.91  -1.400, "Medical Director", 4, 0.1, 0.1)
				Draw3DText( 315.9, -1467.53, 37.91  -1.600, "T. Pickles", 4, 0.1, 0.1)
				Draw3DText( 315.9, -1467.53, 37.91  -1.800, "", 4, 0.1, 0.1)		
			end	

			--Deputy Medical Directors Office
			if GetDistanceBetweenCoords( 310.41, -1463.08, 37.91, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 310.41, -1463.08, 37.91  -1.400, "Deputy Medical Director", 4, 0.1, 0.1)
				Draw3DText( 310.41, -1463.08, 37.91  -1.600, "W. Woodard", 4, 0.1, 0.1)
				Draw3DText( 310.41, -1463.08, 37.91  -1.800, "", 4, 0.1, 0.1)		
			end	

			--Med One Office
			if GetDistanceBetweenCoords( 316.96, -1471.13, 37.91, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 316.96, -1471.13, 37.91  -1.400, "MED ONE", 4, 0.1, 0.1)
				Draw3DText( 316.96, -1471.13, 37.91 -1.600, "Flight Director", 4, 0.1, 0.1)
				Draw3DText( 316.96, -1471.13, 37.91  -1.800, "J. Fulton", 4, 0.1, 0.1)		
			end	

			--Dispatch Director Office
			if GetDistanceBetweenCoords( 444.76, -980.12, 26.67, GetEntityCoords(GetPlayerPed(-1))) < 2.5 then
				Draw3DText( 444.76, -980.12, 26.67  -1.400, "Dispatch Director", 4, 0.1, 0.1)
				Draw3DText( 444.76, -980.12, 26.67 -1.600, "S. Super", 4, 0.1, 0.1)
				Draw3DText( 444.76, -980.12, 26.67  -1.800, "", 4, 0.1, 0.1)		
			end	

			--DMV 1
			if GetDistanceBetweenCoords( -215.43, -1916.77, 28.82, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( -215.43, -1916.77, 28.82  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( -215.43, -1916.77, 28.82  -1.600, "THIS BOOTH", 4, 0.1, 0.1)
				Draw3DText( -215.43, -1916.77, 28.82  -1.800, "OPEN", 4, 0.1, 0.1)		
			end				
			
			--DMV 2
			if GetDistanceBetweenCoords( -213.09, -1915.34, 28.82, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( -213.09, -1915.34, 28.82  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( -213.09, -1915.34, 28.82  -1.600, "THIS BOOTH", 4, 0.1, 0.1)
				Draw3DText( -213.09, -1915.34, 28.82  -1.800, "CLOSED", 4, 0.1, 0.1)		
			end	
			
			--DMV 3
			if GetDistanceBetweenCoords( -210.45, -1913.5, 28.83, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( -210.45, -1913.5, 28.83  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( -210.45, -1913.5, 28.83 -1.600, "THIS BOOTH", 4, 0.1, 0.1)
				Draw3DText( -210.45, -1913.5, 28.83  -1.800, "CLOSED", 4, 0.1, 0.1)		
			end				
		
			--Mission Row Parking spot 1
			if GetDistanceBetweenCoords( 438.4, -1018.3, 27.7, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 438.4, -1018.3, 27.7  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 438.4, -1018.3, 27.7 -1.600, "Park Here", 4, 0.1, 0.1)
				Draw3DText( 438.4, -1018.3, 27.7  -1.800, "", 4, 0.1, 0.1)		
			end	

			--Mission Row Parking spot 2
			if GetDistanceBetweenCoords( -441.0, -1024.2, 28.3, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 441.0, -1024.2, 28.3  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 441.0, -1024.2, 28.3 -1.600, "Park Here", 4, 0.1, 0.1)
				Draw3DText( 441.0, -1024.2, 28.3  -1.800, "", 4, 0.1, 0.1)		
			end	

			--Mission Row Parking spot 3
			if GetDistanceBetweenCoords( 453.5, -1022.2, 28.0, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 453.5, -1022.2, 28.0  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 453.5, -1022.2, 28.0 -1.600, "Park Here", 4, 0.1, 0.1)
				Draw3DText( 453.5, -1022.2, 28.0  -1.800, "", 4, 0.1, 0.1)		
			end	


			--Mission Row Parking spot 4
			if GetDistanceBetweenCoords( 450.9, -1016.5, 28.1, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 450.9, -1016.5, 28.1  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 450.9, -1016.5, 28.1 -1.600, "Park Here", 4, 0.1, 0.1)
				Draw3DText( 450.9, -1016.5, 28.1  -1.800, "", 4, 0.1, 0.1)		
			end	

			--City Hall
			if GetDistanceBetweenCoords( 237.75, -414.22, 47.95, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 237.75, -414.22, 47.95  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 237.75, -414.22, 47.95 -1.600, "City Hall", 4, 0.1, 0.1)
				Draw3DText( 237.75, -414.22, 47.95  -1.800, "", 4, 0.1, 0.1)		
			end	

			--Job Center
			if GetDistanceBetweenCoords( 236.83, -416.62, 47.95, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 236.83, -416.62, 47.95  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 236.83, -416.62, 47.95 -1.600, "Job Center", 4, 0.1, 0.1)
				Draw3DText( 236.83, -416.62, 47.95  -1.800, "", 4, 0.1, 0.1)		
			end	

			--[[

===================================================================================================================
==========================================POLICE TRAINING CENTER===================================================
===================================================================================================================

]]--

				-- LEO Training Facility - Gate 1
		if GetDistanceBetweenCoords( -2319.18, 3399.4, 38.0, GetEntityCoords(GetPlayerPed(-1))) < 200.0 then
			Draw3DText( -2319.18, 3399.4, 38.0  -0.800, "State of Colorado", 1, 0.7, 0.7)
			Draw3DText( -2319.18, 3399.4, 38.0  -2.000, "Emergency Services Training Facility", 1, 0.7, 0.7)
			Draw3DText( -2319.18, 3399.4, 38.0  -3.200, "Main Gate", 1, 0.7, 0.7)
		end

				-- LEO Training Facility - Gate 2
		if GetDistanceBetweenCoords( -1576.73, 2780.74, 24.89, GetEntityCoords(GetPlayerPed(-1))) < 200.0 then
			Draw3DText( -1576.73, 2780.74, 24.89  -0.800, "State of Colorado", 1, 0.7, 0.7)
			Draw3DText( -1576.73, 2780.74, 24.89  -2.000, "Emergency Services Training Facility", 1, 0.7, 0.7)
			Draw3DText( -1576.73, 2780.74, 24.89  -3.200, "Back Gate", 1, 0.7, 0.7)
		end

				-- LEO Training Facility - Weapons Training Room
		if GetDistanceBetweenCoords( -2145.72, 3244.33, 65.0, GetEntityCoords(GetPlayerPed(-1))) < 200.0 then
			Draw3DText( -2145.72, 3244.33, 65.0  -0.800, "Colorado", 1, 0.7, 0.7)
			Draw3DText( -2145.72, 3244.33, 65.0  -2.000, "Weapons Training Facility", 1, 0.7, 0.7)		
		end

				-- LEO Weapons Facility - Range 2
		if GetDistanceBetweenCoords( -2119.82, 3268.78, 33.0, GetEntityCoords(GetPlayerPed(-1))) < 20.0 then
			Draw3DText( -2119.82, 3268.78, 33.0  -0.800, "Range 2", 1, 0.1, 0.1)		
		end

				-- LEO Weapons Facility - Range 1
		if GetDistanceBetweenCoords( -2130.32, 3275.01, 33.0, GetEntityCoords(GetPlayerPed(-1))) < 20.0 then
			Draw3DText( -2130.32, 3275.01, 33.0  -0.800, "Range 1", 1, 0.1, 0.1)		
		end

				-- LEO Weapons Facility - Exit
		if GetDistanceBetweenCoords( -2133.5, 3304.14, 34.50, GetEntityCoords(GetPlayerPed(-1))) < 60.0 then
			Draw3DText( -2133.5, 3304.14, 34.50  -0.800, "EXIT HERE", 1, 0.3, 0.3)		
		end

				-- LEO Weapons Facility - Enter
		if GetDistanceBetweenCoords( -2150.13, 3235.76, 34.50, GetEntityCoords(GetPlayerPed(-1))) < 60.0 then
			Draw3DText( -2150.13, 3235.76, 34.50  -0.800, "ENTER HERE", 1, 0.3, 0.3)		
		end

				-- LEO Weapons Facility - Enter
		if GetDistanceBetweenCoords( -2340.2, 3264.95, 34.50, GetEntityCoords(GetPlayerPed(-1))) < 60.0 then
			Draw3DText( -2340.2, 3264.95, 34.50  -0.600, "SWAT/SRT TRAINING", 1, 0.3, 0.3)	
			Draw3DText( -2340.2, 3264.95, 34.50  -1.200, "FACILITY", 1, 0.3, 0.3)	
		end

--[[

===================================================================================================================
==========================================POLICE TRAINING CENTER===================================================
===================================================================================================================

]]--
	end
end

-------------------------------------------------------------------------------------------------------------------------
function Draw3DText(x,y,z,textInput,fontId,scaleX,scaleY)
         local px,py,pz=table.unpack(GetGameplayCamCoords())
         local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)    
         local scale = (1/dist)*20
         local fov = (1/GetGameplayCamFov())*100
         local scale = scale*fov   
         SetTextScale(scaleX*scale, scaleY*scale)
         SetTextFont(fontId)
         SetTextProportional(1)
         SetTextColour(250, 250, 250, 255)		-- You can change the text color here
         SetTextDropshadow(1, 1, 1, 1, 255)
         SetTextEdge(2, 0, 0, 0, 150)
         SetTextDropShadow()
         SetTextOutline()
         SetTextEntry("STRING")
         SetTextCentre(1)
         AddTextComponentString(textInput)
         SetDrawOrigin(x,y,z+2, 0)
         DrawText(0.0, 0.0)
         ClearDrawOrigin()
        end
