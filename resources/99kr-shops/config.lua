Config = {

    Locations = {
            --24/7 Innocence Blvd
        [1] = {
            ["shelfs"] = {
                {["x"] = 25.73, ["y"] = -1347.27, ["z"] = 29.5, ["value"] = "checkout"},
                {["x"] = 27.50, ["y"] = -1345.25, ["z"] = 29.5, ["value"] = "drinks"},
                {["x"] = 28.99, ["y"] = -1342.62, ["z"] = 29.5, ["value"] = "snacks"},
                {["x"] = 32.45, ["y"] = -1342.96, ["z"] = 29.5, ["value"] = "readymeal"},
                {["x"] = 25.67, ["y"] = -1344.99, ["z"] = 29.5, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 29.41, ["y"] = -1345.01, ["z"] = 29.5
            },
            ["cashier"] = {
                ["x"] = 24.44, ["y"] = -1347.34, ["z"] = 28.5, ["h"] = 270.82
            },
        },

            --LTD Grove St
        [2] = {
            ["shelfs"] = {
                {["x"] = -48.37, ["y"] = -1757.93, ["z"] = 29.42, ["value"] = "checkout"},
                {["x"] = -54.67, ["y"] = -1748.58, ["z"] = 29.42, ["value"] = "drinks"},
                {["x"] = -52.80, ["y"] = -1753.28, ["z"] = 29.42, ["value"] = "snacks"},
                {["x"] = -50.08, ["y"] = -1749.24, ["z"] = 29.42, ["value"] = "readymeal"},
                {["x"] = -47.25, ["y"] = -1756.58, ["z"] = 29.42, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -48.34, ["y"] = -1752.72, ["z"] = 29.42
            },
            ["cashier"] = {
                ["x"] = -47.38, ["y"] = -1758.7, ["z"] = 28.44, ["h"] = 48.84
            },
        },

            --Robs Liquor San Andreas Ave
        [3] = {
            ["shelfs"] = {
                {["x"] = -1222.26, ["y"] = -906.86, ["z"] = 12.33, ["value"] = "checkout"},
                {["x"] = -1224.09, ["y"] = -908.13, ["z"] = 12.33, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -1220.78, ["y"] = -909.19, ["z"] = 12.33
            },
            ["cashier"] = {
                ["x"] = -1221.47, ["y"] = -907.99, ["z"] = 11.36, ["h"] = 28.09,
                ["hash"] = "s_m_m_linecook"
            },
        },

            --Robs Liquor Prosperity St
        [4] = {
            ["shelfs"] = {
                {["x"] = -1487.62, ["y"] = -378.60, ["z"] = 40.16, ["value"] = "checkout"},
                {["x"] = -1486.07, ["y"] = -380.21, ["z"] = 40.16, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -1485.59, ["y"] = -376.7, ["z"] = 40.16
            },
            ["cashier"] = {
                ["x"] = -1486.75, ["y"] = -377.51, ["z"] = 39.18, ["h"] = 130.0,
                ["hash"] = "s_m_m_linecook"
            },
        },

            --LTD Lindsay Circus
        [5] = {
            ["shelfs"] = {
                {["x"] = -707.31, ["y"] = -914.66, ["z"] = 19.22, ["value"] = "checkout"},
                {["x"] = -718.20, ["y"] = -911.52, ["z"] = 19.22, ["value"] = "drinks"},
                {["x"] = -713.68, ["y"] = -913.90, ["z"] = 19.22, ["value"] = "snacks"},
                {["x"] = -714.20, ["y"] = -909.15, ["z"] = 19.22, ["value"] = "readymeal"},
                {["x"] = -707.36, ["y"] = -912.83, ["z"] = 19.22, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -711.01, ["y"] = -911.25, ["z"] = 19.22
            },
            ["cashier"] = {
                ["x"] = -706.13, ["y"] = -914.52, ["z"] = 18.24, ["h"] = 90.0
            },
        },

            --Robs Liquor El Rancho Blvd
        [6] = {
            ["shelfs"] = {
                {["x"] = 1135.7, ["y"] = -982.79, ["z"] = 46.42, ["value"] = "checkout"},
                {["x"] = 1135.3, ["y"] = -980.55, ["z"] = 46.42, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1132.94, ["y"] = -983.19, ["z"] = 46.42
            },
            ["cashier"] = {
                ["x"] = 1134.27, ["y"] = -983.16, ["z"] = 45.44, ["h"] = 280.08,
                ["hash"] = "s_m_m_linecook"
            },
        },

            --24/7 Clinton Ave
        [7] = {
            ["shelfs"] = {
                {["x"] = 373.55, ["y"] = 325.52, ["z"] = 103.57, ["value"] = "checkout"},
                {["x"] = 376.03, ["y"] = 327.65, ["z"] = 103.57, ["value"] = "drinks"},
                {["x"] = 378.15, ["y"] = 329.83, ["z"] = 103.57, ["value"] = "snacks"},
                {["x"] = 381.29, ["y"] = 328.64, ["z"] = 103.57, ["value"] = "readymeal"},
                {["x"] = 374.17, ["y"] = 327.92, ["z"] = 103.57, ["value"] = "diverse"},
            }, 
            ["blip"] = {
                ["x"] = 378.8, ["y"] = 329.64, ["z"] = 103.57
            },
            ["cashier"] = {
                ["x"] = 372.54, ["y"] = 326.38, ["z"] = 102.59, ["h"] = 257.27
            }
        },

            --LTD Mirror Park
        [8] = {
            ["shelfs"] = {
                {["x"] = 1163.67, ["y"] = -323.92, ["z"] = 69.21, ["value"] = "checkout"},
                {["x"] = 1152.45, ["y"] = -322.75, ["z"] = 69.21, ["value"] = "drinks"},
                {["x"] = 1157.31, ["y"] = -324.37, ["z"] = 69.21, ["value"] = "snacks"},
                {["x"] = 1156.00, ["y"] = -319.68, ["z"] = 69.21, ["value"] = "readymeal"},
                {["x"] = 1163.33, ["y"] = -322.25, ["z"] = 69.21, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1157.88, ["y"] = -319.42, ["z"] = 69.21
            },
            ["cashier"] = {
                ["x"] = 1164.85, ["y"] = -323.67, ["z"] = 68.23, ["h"] = 98.12
            },
        },

            --24/7 Palomino Fwy
        [9] = {
            ["shelfs"] = {
                {["x"] = 2557.44, ["y"] = 382.03, ["z"] = 108.62, ["value"] = "checkout"},
                {["x"] = 2555.28, ["y"] = 383.96, ["z"] = 108.62, ["value"] = "drinks"},
                {["x"] = 2552.65, ["y"] = 385.58, ["z"] = 108.62, ["value"] = "snacks"},
                {["x"] = 2553.23, ["y"] = 389.04, ["z"] = 108.62, ["value"] = "readymeal"},
                {["x"] = 2555.08, ["y"] = 382.18, ["z"] = 108.64, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 2552.75, ["y"] = 386.28, ["z"] = 108.62
            },
            ["cashier"] = {
                ["x"] = 2557.27, ["y"] = 380.81, ["z"] = 107.64, ["h"] = 0.0
            },
        },

            --24/7 Ineseno Rd
        [10] = {
            ["shelfs"] = {
                {["x"] = -3039.16, ["y"] = 585.71, ["z"] = 7.91, ["value"] = "checkout"},
                {["x"] = -3041.83, ["y"] = 586.86, ["z"] = 7.91, ["value"] = "drinks"},
                {["x"] = -3044.86, ["y"] = 587.45, ["z"] = 7.91, ["value"] = "snacks"},
                {["x"] = -3045.56, ["y"] = 590.78, ["z"] = 7.91, ["value"] = "readymeal"},
                {["x"] = -3041.03, ["y"] = 585.11, ["z"] = 7.91, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -3045.01, ["y"] = 588.14, ["z"] = 7.91
            },
            ["cashier"] = {
                ["x"] = -3038.96, ["y"] = 584.53, ["z"] = 6.93, ["h"] = 0.0
            },
        },

            --24/7 Barbareno Rd
        [11] = {
            ["shelfs"] = {
                {["x"] = -3242.11, ["y"] = 1001.20, ["z"] = 12.83, ["value"] = "checkout"},
                {["x"] = -3244.07, ["y"] = 1003.14, ["z"] = 12.83, ["value"] = "drinks"},
                {["x"] = -3246.58, ["y"] = 1004.95, ["z"] = 12.83, ["value"] = "snacks"},
                {["x"] = -3245.88, ["y"] = 1008.5, ["z"] = 12.83, ["value"] = "readymeal"},
                {["x"] = -3243.89, ["y"] = 1001.32, ["z"] = 12.84, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -3246.45, ["y"] = 1005.64, ["z"] = 12.83
            },
            ["cashier"] = {
                ["x"] = -3242.24, ["y"] = 1000.0, ["z"] = 11.85, ["h"] = 353.5
            },
        },

            --Robs Liquor Great Ocean Hwy
        [12] = {
            ["shelfs"] = {
                {["x"] = -2967.78, ["y"] = 391.49, ["z"] = 15.04, ["value"] = "checkout"},
                {["x"] = -2967.87, ["y"] = 389.3, ["z"] = 15.04, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -2964.96, ["y"] = 391.33, ["z"] = 15.04
            },
            ["cashier"] = {
                ["x"] = -2966.38, ["y"] = 391.44, ["z"] = 14.06, ["h"] = 91.62,
                ["hash"] = "s_m_m_linecook"
            },
        },

            --LTD Banham Canyon Dr
        [13] = {
            ["shelfs"] = {
                {["x"] = -1820.38, ["y"] = 792.69, ["z"] = 138.11, ["value"] = "checkout"},
                {["x"] = -1830.41, ["y"] = 787.62, ["z"] = 138.33, ["value"] = "drinks"},
                {["x"] = -1825.52, ["y"] = 789.33, ["z"] = 138.23, ["value"] = "snacks"},
                {["x"] = -1829.13, ["y"] = 792.0, ["z"] = 138.26, ["value"] = "readymeal"},
                {["x"] = -1821.55, ["y"] = 793.97, ["z"] = 138.12, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = -1827.64, ["y"] = 793.31, ["z"] = 138.22
            },
            ["cashier"] = {
                ["x"] = -1819.53, ["y"] = 793.55, ["z"] = 137.11, ["h"] = 129.05,
            },
        },

            --24/7 Route 68
        [14] = {
            ["shelfs"] = {
                {["x"] = 547.75, ["y"] = 2671.53, ["z"] = 42.16, ["value"] = "checkout"},
                {["x"] = 546.33, ["y"] = 2668.85, ["z"] = 42.16, ["value"] = "drinks"},
                {["x"] = 545.17, ["y"] = 2666.05, ["z"] = 42.16, ["value"] = "snacks"},
                {["x"] = 541.8, ["y"] = 2666.06, ["z"] = 42.16, ["value"] = "readymeal"},
                {["x"] = 548.08, ["y"] = 2669.36, ["z"] = 42.16, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 544.43, ["y"] = 2666.07, ["z"] = 42.16
            },
            ["cashier"] = {
                ["x"] = 549.04, ["y"] = 2671.36, ["z"] = 41.18, ["h"] = 98.25
            },
        },

            --Scoops Liquor Barn Route 68
        [15] = {
            ["shelfs"] = {
                {["x"] = 1165.36, ["y"] = 2709.45, ["z"] = 38.16, ["value"] = "checkout"},
                {["x"] = 1167.64, ["y"] = 2709.41, ["z"] = 38.16, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1167.02, ["y"] = 2711.82, ["z"] = 38.16
            },
            ["cashier"] = {
                ["x"] = 1165.29, ["y"] = 2710.79, ["z"] = 37.18, ["h"] = 176.18,
                ["hash"] = "s_m_m_linecook"
            },
        },

            --24/7 Senora Fwy
        [16] = {
            ["shelfs"] = {
                {["x"] = 2678.82, ["y"] = 3280.36, ["z"] = 55.24, ["value"] = "checkout"},
                {["x"] = 2677.8, ["y"] = 3283.08, ["z"] = 55.24, ["value"] = "drinks"},
                {["x"] = 2676.17, ["y"] = 3285.7, ["z"] = 55.24, ["value"] = "snacks"},
                {["x"] = 2678.1, ["y"] = 3288.43, ["z"] = 55.24, ["value"] = "readymeal"},
                {["x"] = 2676.91, ["y"] = 3281.38, ["z"] = 55.24, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 2676.55, ["y"] = 3286.27, ["z"] = 55.24
            },
            ["cashier"] = {
                ["x"] = 2678.1, ["y"] = 3279.4, ["z"] = 54.26, ["h"] = 331.07
            },
        },

            --24/7 Alhambra Dr
        [17] = {
            ["shelfs"] = {
                {["x"] = 1961.17, ["y"] = 3740.5, ["z"] = 32.34, ["value"] = "checkout"},
                {["x"] = 1961.74, ["y"] = 3743.33, ["z"] = 32.34, ["value"] = "drinks"},
                {["x"] = 1961.68, ["y"] = 3746.29, ["z"] = 32.34, ["value"] = "snacks"},
                {["x"] = 1964.74, ["y"] = 3747.71, ["z"] = 32.34, ["value"] = "readymeal"},
                {["x"] = 1960.18, ["y"] = 3742.21, ["z"] = 32.36, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1962.33, ["y"] = 3746.67, ["z"] = 32.34
            },
            ["cashier"] = {
                ["x"] = 1960.13, ["y"] = 3739.94, ["z"] = 31.36, ["h"] = 297.89
            },
        },

            --Liquor Ace Algonquin Blvd
        [18] = {
            ["shelfs"] = {
                {["x"] = 1393.13, ["y"] = 3605.2, ["z"] = 34.98, ["value"] = "checkout"},
                {["x"] = 1390.93, ["y"] = 3604.4, ["z"] = 35.0, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1391.23, ["y"] = 3609.29, ["z"] = 34.98
            },
            ["cashier"] = {
                ["x"] = 1392.74, ["y"] = 3606.35, ["z"] = 34.0, ["h"] = 202.73,
                ["hash"] = "s_m_m_linecook"
            },
        },

            --LTD Grapeseed Main St
        [19] = {
            ["shelfs"] = {
                {["x"] = 1697.92, ["y"] = 4924.46, ["z"] = 42.06, ["value"] = "checkout"},
                {["x"] = 1706.63, ["y"] = 4931.63, ["z"] = 42.06, ["value"] = "drinks"},
                {["x"] = 1702.28, ["y"] = 4928.93, ["z"] = 42.06, ["value"] = "snacks"},
                {["x"] = 1706.43, ["y"] = 4927.02, ["z"] = 42.06, ["value"] = "readymeal"},
                {["x"] = 1699.44, ["y"] = 4923.41, ["z"] = 42.06, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1705.22, ["y"] = 4925.39, ["z"] = 42.06
            },
            ["cashier"] = {
                ["x"] = 1697.3, ["y"] = 4923.47, ["z"] = 41.08, ["h"] = 323.98
            },
        },

            --24/7 Senora Fwy
        [20] = {
            ["shelfs"] = {
                {["x"] = 1728.78, ["y"] = 6414.41, ["z"] = 35.04, ["value"] = "checkout"},
                {["x"] = 1731.44, ["y"] = 6415.73, ["z"] = 35.04, ["value"] = "drinks"},
                {["x"] = 1733.92, ["y"] = 6417.4, ["z"] = 35.04, ["value"] = "snacks"},
                {["x"] = 1736.88, ["y"] = 6415.61, ["z"] = 35.04, ["value"] = "readymeal"},
                {["x"] = 1729.82, ["y"] = 6416.42, ["z"] = 35.04, ["value"] = "diverse"},
            },
            ["blip"] = {
                ["x"] = 1734.64, ["y"] = 6417.04, ["z"] = 35.04
            },
            ["cashier"] = {
                ["x"] = 1727.87, ["y"] = 6415.25, ["z"] = 34.06, ["h"] = 242.93
            },
        },
        
            --Pillbox Giftshop
            [21] = {
                ["shelfs"] = {
                    {["x"] = 316.54, ["y"] = -588.12, ["z"] = 43.29, ["value"] = "checkout"},
                    {["x"] = 315.1, ["y"] = -588.83, ["z"] = 43.29, ["value"] = "drinks"},
                    {["x"] = 318.27, ["y"] = -587.86, ["z"] = 43.29, ["value"] = "snacks"},
                },
                --[[["blip"] = {
                    ["x"] = -48.34, ["y"] = -1752.72, ["z"] = 29.42
                },]]--
                ["cashier"] = {
                    ["x"] = 316.43, ["y"] = -587.09, ["z"] = 42.3, ["h"] = 205.87,
                    ["hash"] = "a_f_y_business_02"
                },
            },

             --Police Shop Mission Row
             [22] = {
                ["shelfs"] = {
                    {["x"] = 468.12, ["y"] = -990.23, ["z"] = 30.69, ["value"] = "checkout"},
                    {["x"] = 463.42, ["y"] = -994.5, ["z"] = 30.03, ["value"] = "police"},
                },
                --[[["blip"] = {
                    ["x"] = -48.34, ["y"] = -1752.72, ["z"] = 29.42
                },]]--
            },
            
             --Police Shop Sandy Shores
             [23] = {
                ["shelfs"] = {
                    {["x"] = 1853.57, ["y"] = 3690.6, ["z"] = 34.26, ["value"] = "checkout"},
                    {["x"] = 1845.22, ["y"] = 3694.19, ["z"] = 34.26, ["value"] = "police"},
                },
                --[[["blip"] = {
                    ["x"] = -48.34, ["y"] = -1752.72, ["z"] = 29.42
                },]]--
            },  

             --Police Shop Sandy Shores
             [24] = {
                ["shelfs"] = {
                    {["x"] = -449.73, ["y"] = 6010.11, ["z"] = 31.72, ["value"] = "checkout"},
                    {["x"] = -443.65, ["y"] = 6004.27, ["z"] = 31.72, ["value"] = "police"},
                },
                --[[["blip"] = {
                    ["x"] = -48.34, ["y"] = -1752.72, ["z"] = 29.42
                },]]--
            },  

               --City Hall
              [25] = {
                ["shelfs"] = {
                    {["x"] = -925.39, ["y"] = -2036.41, ["z"] = 9.4, ["value"] = "checkout"},
                    {["x"] = -926.43, ["y"] = -2037.5, ["z"] = 9.4, ["value"] = "cityhall"},
                },
                --[[["blip"] = {
                    ["x"] = -48.34, ["y"] = -1752.72, ["z"] = 29.42
                },]]--
            },   
            
              --DMV
              [26] = {
                ["shelfs"] = {
                    {["x"] = -925.39, ["y"] = -2036.41, ["z"] = 9.4, ["value"] = "checkout"},
                    {["x"] = -928.75, ["y"] = -2039.53, ["z"] = 9.4, ["value"] = "dmv"},
                },
                --[[["blip"] = {
                    ["x"] = -48.34, ["y"] = -1752.72, ["z"] = 29.42
                },]]--
                ["cashier"] = {
                    ["x"] = -926.48, ["y"] = -2034.9, ["z"] = 8.4, ["h"] = 216.05,
                    ["hash"] = "a_m_m_prolhost_01"
            }, 
        },              
    },

    --[[
        
    U can specify the hash of the cashier like this:
        ["cashier"] = {
            ["x"] = 0.0, ["y"] = 0.0, ["z"] = 0.0, ["h"] = 0.0,     (h = heading)
            ["hash"] = "mp_m_freemode_01"
        },
        or ["hash"] = 1885233650
    ]]--
    
    Locales = {
        ["checkout"] = "Cash Register",
        ["drinks"] = "Drinks",
        ["snacks"] = "Snacks",
        ["readymeal"] = "Ready meal",
        ["diverse"] = "Diverse Stuff",
        ["police"] = 'Police Shop',
        ["cityhall"] = 'City Hall',
        ["dmv"] = 'DMV',
    },

    Items = {
        ["drinks"] = {
            {label = "Coca Cola", item = "cocacola", price = 5},
            {label = "Fanta Exotic", item = "fanta", price = 5},
            {label = "Sprite", item = "sprite", price = 5},
            {label = "Loka Crush", item = "loka", price = 5},
        },
        ["snacks"] = {
            {label = "Cheese Doodles", item = "cheesebows", price = 7},
            {label = "Chips", item = "chips", price = 7},
            {label = "Milk Chocolate ", item = "marabou", price = 7},
        },
        ["readymeal"] = {
            {label = "Kebab Pizza", item = "pizza", price = 10},
            {label = "Bacon Burger", item = "baconburger", price = 10},
            {label = "Pasta Carbonara", item = "pastacarbonara", price = 10},
            {label = "Ham sammy", item = "macka", price = 8},
        },
        ["diverse"] = {
            {label = "Trisslott", item = "lotteryticket", price = 20},
            {label = "NOS", item = "nitrocannister", price = 40},
            {label = "Wrench", item = "wrench", price = 5},
            {label = "GPS", item = "gps", price = 30},
            {label = "Radio", item = "radio", price = 1},
            {label = "Radio Detector", item = "radardetector", price = 300},
            {label = "rope", item = "rope", price = 5},
            --{label = "carjack", item = "CarJack", price = 5},
        },
        ["police"] = {
            {label = "Coffee", item = "coffee", price = 1},
            {label = "Policeman's Best Friend", item = "donut", price = 1},
            {label = "Ammo", item = "clip", price = 1},
            {label = "Armor", item = "armour", price = 1},
            {label = "Medikit", item = "medikit", price = 1},
            {label = "GPS", item = "gps", price = 1},
            {label = "Radio", item = "radio", price = 1},
            {label = "handcuffs", item = "handcuffs", price = 1},
        },
        ["cityhall"] = {
            {label = "Class 1 Weapons License", item = "weapons_license1", price = 100},
            {label = "Class 2 Weapons License", item = "weapons_license2", price = 200},
            {label = "Class 3 Weapons License", item = "weapons_license3", price = 300},
            {label = "Hunting License", item = "hunting_license", price = 100},
            {label = "Fishing License", item = "fishing_license", price = 100},
            {label = "Diving License", item = "diving_license", price = 50},
            {label = "Marriage License", item = "marriage_license", price = 5000},
        },
        ["dmv"] = {
            {label = "Boating License", item = "boating_license", price = 40},
            {label = "Taxi License", item = "taxi_license", price = 175},
            {label = "Pilot License", item = "pilot_license", price = 500},
            {label = "Commerical Drivers License", item = "commercial_drivers_license", price = 50},
            {label = "Motorcycle License", item = "motorcycle_license", price = 15},
            {label = "Drivers License", item = "driver_license", price = 35},
            {label = "License plate", item = "licenseplate", price = 50},
        },
    },
}