Locales['fr'] = {

    ['firstaidkit']         = 'First aid kit',
    ['defibrillateur']      = 'defibrillator',

    ['yes']                 = 'Yes',
    ['no']                  = 'No',
    ['press_menu']          = ' Press ~ INPUT_CONTEXT ~ to access the store',
    ['drugstore']           = 'Pharmacy',
    ['valid_this_purchase'] = 'Validate your purchase',
    ['not_enough_money']    = 'You do not have enough money',
    ['bought']              = 'You bought a ~ g ~',
    ['use_firstaidkit']     = 'You used a ~ g ~ first aid kit',
    ['has_ambulance']       = '~ r ~ You can only use this kit, if there is not an ambulance in service',

    ['no_players']          = 'No players nearby',
    ['revive_inprogress']   = 'Resuscitation in progress',
    ['revive_complete']     = 'You have revived',
    ['isdead']              = 'has succumbed',
    ['unconscious']         = 'is not unconscious'

}