Config                            = {}

Config.DrawDistance               = 100.0

Config.Marker                     = { type = 1, x = 1, y = 1, z = 1, r = 102, g = 0, b = 102, rotate = true } -- pillbox

Config.ReviveReward               = 700  -- revive reward, set to 0 if you don't want it enabled
Config.AntiCombatLog              = true -- enable anti-combat logging?
Config.LoadIpl                    = false -- disable if you're using fivem-ipl or other IPL loaders

Config.Locale                     = 'en'

local second = 1000
local minute = 60 * second

Config.EarlyRespawnTimer          = 2 * minute  -- Time til respawn is available
Config.BleedoutTimer              = 10 * minute -- Time til the player bleeds out

Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false

Config.RemoveWeaponsAfterRPDeath  = false
Config.RemoveCashAfterRPDeath     = false
Config.RemoveItemsAfterRPDeath    = false

-- Let the player pay for respawning early, only if he can afford it.
Config.EarlyRespawnFine           = false
Config.EarlyRespawnFineAmount     = 5000

Config.RespawnPoint = { coords = vector3(320.052, -593.575, 43.292), heading = 93.42} -- pillbox

Config.Hospitals = {

	CentralLosSantos = {

		Blip = {
			coords = vector3(351.55, -587.33, 74.17),
			sprite = 61,
			scale  = 1.2,
			color  = 2
		},

		AmbulanceActions = {
			vector3(335.93, -580.58, 28.79)
		},

		Pharmacies = {
			vector3(353.98, -592.53, 43.31),
			vector3(435.84, -973.83, 25.67)
		},

		Vehicles = {
			{
				Spawner = vector3(316.39, -560.34, 28.74),
				InsideShop = vector3(289.532,-584.539, 42.832), --pillbox
				Marker = { type = 36, x = 1.0, y = 1.0, z = 1.0, r = 100, g = 50, b = 200, a = 100, rotate = true },
				SpawnPoints = { -- all pillbox
					{ coords = vector3(292.516,-609.973,43.042), heading = 68.186, radius = 4.0 }, 
					{ coords = vector3(287.627,-589.442,42.813), heading = 342.019, radius = 4.0 },
					{ coords = vector3(291.625,-571.292, 42.853), heading = 69.296, radius = 6.0 }
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(317.5, -1449.5, 46.5),
				InsideShop = vector3(305.6, -1419.7, 41.5),
				Marker = { type = 34, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = true },
				SpawnPoints = {
					{ coords = vector3(313.5, -1465.1, 46.5), heading = 142.7, radius = 10.0 },
					{ coords = vector3(299.5, -1453.2, 46.5), heading = 142.7, radius = 10.0 }
				}
			}
		},

		FastTravels = {
			{
				From = vector3(294.7, -1448.1, 29.0),
				To = { coords = vector3(272.8, -1358.8, 23.5), heading = 0.0 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(275.3, -1361, 23.5),
				To = { coords = vector3(295.8, -1446.5, 28.9), heading = 0.0 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(247.3, -1371.5, 23.5),
				To = { coords = vector3(333.1, -1434.9, 45.5), heading = 138.6 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(335.5, -1432.0, 45.50),
				To = { coords = vector3(249.1, -1369.6, 23.5), heading = 0.0 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(234.5, -1373.7, 20.9),
				To = { coords = vector3(320.9, -1478.6, 28.8), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 1.0, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(317.9, -1476.1, 28.9),
				To = { coords = vector3(238.6, -1368.4, 23.5), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 1.0, r = 102, g = 0, b = 102, rotate = false }
			}
		},

		FastTravelsPrompt = {
			--Heli
			{
				From = vector3(325.48, -598.74, 42.33),
				To = { coords = vector3(339.05, -584.02, 74.17), heading = 244.92 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false },
				Prompt = _U('fast_travel1')
			},

			{
				From = vector3(339.05, -584.02, 73.17),
				To = { coords = vector3(310.93, -599.21, 43.29), heading = 67.9 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false },
				Prompt = _U('fast_travel3')
			},

			--Garage
			{
				From = vector3(327.99, -594.72, 42.33),
				To = { coords = vector3(319.67, -560.12, 28.74), heading = 23.02 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false },
				Prompt = _U('fast_travel2')
			},

			{
			From = vector3(319.67, -560.12, 27.74),
			To = { coords = vector3(310.93, -599.21, 43.29), heading = 67.9 },
			Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false },
			Prompt = _U('fast_travel3')
			}

		}

	},

	SandyShores = {

		Blip = {
			coords = vector3(351.55, -587.33, 74.17),
			sprite = 61,
			scale  = 1.2,
			color  = 2
		},

		AmbulanceActions = {
			vector3(1830.6, 3676.91, 33.29)
		},

		Pharmacies = {
			vector3(1836.76, 3688.99, 33.29)
		},

		Vehicles = {
			{
				Spawner = vector3(1837.28, 3695.05, 34.27),
				InsideShop = vector3(446.7, -1355.6, 43.5),
				Marker = { type = 36, x = 1.0, y = 1.0, z = 1.0, r = 100, g = 50, b = 200, a = 100, rotate = true },
				SpawnPoints = {
					{ coords = vector3(1833.41, 3697.43, 33.9), heading = 299.31, radius = 4.0 },
					{ coords = vector3(1821.37, 3690.41, 33.9), heading = 300.18, radius = 4.0 }
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(317.5, -1449.5, 46.5),
				InsideShop = vector3(305.6, -1419.7, 41.5),
				Marker = { type = 34, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = true },
				SpawnPoints = {
					{ coords = vector3(313.5, -1465.1, 46.5), heading = 142.7, radius = 10.0 },
					{ coords = vector3(299.5, -1453.2, 46.5), heading = 142.7, radius = 10.0 }
				}
			}
		},

		FastTravels = {
			{
				From = vector3(294.7, -1448.1, 20.0),
				To = { coords = vector3(272.8, -1358.8, 23.5), heading = 0.0 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(275.3, -1361, 20.5),
				To = { coords = vector3(295.8, -1446.5, 28.9), heading = 0.0 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(247.3, -1371.5, 20.5),
				To = { coords = vector3(333.1, -1434.9, 45.5), heading = 138.6 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(335.5, -1432.0, 40.50),
				To = { coords = vector3(249.1, -1369.6, 23.5), heading = 0.0 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(234.5, -1373.7, 20.9),
				To = { coords = vector3(320.9, -1478.6, 28.8), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 1.0, r = 102, g = 0, b = 102, rotate = false }
			},

			{
				From = vector3(317.9, -1476.1, 20.9),
				To = { coords = vector3(238.6, -1368.4, 23.5), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 1.0, r = 102, g = 0, b = 102, rotate = false }
			}
		},

		FastTravelsPrompt = {
			{
				From = vector3(237.4, -1373.8, 26.0),
				To = { coords = vector3(251.9, -1363.3, 38.5), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, rotate = false },
				Prompt = _U('fast_travel')
			},

			{
				From = vector3(256.5, -1357.7, 36.0),
				To = { coords = vector3(235.4, -1372.8, 26.3), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, rotate = false },
				Prompt = _U('fast_travel')
			}
		}

	}
}

Config.AuthorizedVehicles = {

	recruit = {

	},

	cadet = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1}
	},

	probationary = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1}
	},

	emr = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	emt = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	aemt = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	paramedic = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	medone = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1},
		{ model = 'k9', label = 'K9s', price = 1}
	},

	phrn = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	medicalchief = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	deputymedicaldirector = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1},
		{ model = 'EMSC', label = 'Devils Tahoe', price = 1}
	},

	boss = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1},
		{ model = 'sticky', label = 'Stickys SUV', price = 1},
		{ model = 'k9', label = 'K9s', price = 1}
	}

}

Config.AuthorizedHelicopters = {

}
