Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Config.MarkerColor                = { r = 255, g = 0, b = 0 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableNonFreemodePeds      = true -- turn this on if you want custom peds
Config.EnableLicenses             = false -- enable if you're using esx_license

Config.EnableHandcuffTimer        = false -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 15 * 60000 -- 10 mins

Config.EnableJobBlip              = true -- enable blips for colleagues, requires esx_society

Config.ReviveReward               = 1000  -- revive reward, set to 0 if you don't want it enabled

Config.MaxInService               = 10
Config.Locale                     = 'en'

Config.FireStations = {

	LSFD = {

		Blip = {
			Coords  = vector3(1202.4, -1460.1, 34.8),
			Sprite  = 436,
			Display = 4,
			Scale   = 0.8,
			Colour  = 1
		},

		Cloakrooms = {
			vector3(1192.5, -1474.5, 34.0), -- El Burro Heights
			vector3(198.0, -1646.0, 29.0), -- Davis
			vector3(1701.7, 3604.3, 34.9), -- Sandy Shores
			vector3(-379.0, 6118.0, 31.0), -- Paleto Bay
			vector3(-2132.5, 2821.5, 33.1) -- Ft Zancudo

		},

		Armories = {
			vector3(1194.5, -1480.5, 34.0), -- El Burro Heights
			vector3(196.0, -1652.0, 29.0), -- Davis
			vector3(1692.0, 3586.0, 34.5), -- Sandy Shores
			vector3(-383.0, 6114.0, 31.0), -- Paleto Bay
			vector3(-2095.6, 2829.5, 32.0) -- Ft Zancudo
		},

		Vehicles = {
			{ -- El Burro Heights
				Spawner = vector3(1508.7, -1480.9, 33.9),
				InsideShop = vector3(482.7, 4811.8, -57.3),
				SpawnPoints = {
					{ coords = vector3(1204.8, -1468.8, 34.0), heading = 1.0, radius = 3.0 },
					{ coords = vector3(1200.8, -1468.8, 34.0), heading = 1.0, radius = 3.0 },
					{ coords = vector3(1196.8, -1468.8, 34.0), heading = 1.0, radius = 3.0 },
					{ coords = vector3(1196.8, -1453.2, 34.0), heading = 1.0, radius = 3.0 }
				}
			},

			{ -- Davis
				Spawner = vector3(207.3, -1662.2, 29.8),
				InsideShop = vector3(482.7, 4811.8, -57.3),
				SpawnPoints = {
					{ coords = vector3(211.3, -1651.1, 29.0), heading = 320.0, radius = 3.0 },
					{ coords = vector3(208.7, -1647.6, 29.0), heading = 230.0, radius = 3.0 },
					{ coords = vector3(214.2, -1634.7, 29.0), heading = 320.0, radius = 3.0 }
				}
			},

			{ -- Sandy Shores
				Spawner = vector3(1699.7, 3598.0, 35.9),
				InsideShop = vector3(482.7, 4811.8, -57.3),
				SpawnPoints = {
					{ coords = vector3(1708.3, 3593.0, 35.0), heading = 212.0, radius = 3.0 },
					{ coords = vector3(1712.5, 3594.9, 35.0), heading = 212.0, radius = 3.0 },
					{ coords = vector3(1717.3, 3597.2, 35.0), heading = 121.0, radius = 3.0 }
				}
			},

			{ -- Paleto Bay
				Spawner = vector3(-377.1, 6121.1, 31.5),
				InsideShop = vector3(482.7, 4811.8, -57.3),
				SpawnPoints = {
					{ coords = vector3(-375.2, 6126.1, 30.5), heading = 45.0, radius = 3.0 },
					{ coords = vector3(-371.6, 6129.1, 30.5), heading = 45.0, radius = 3.0 }
				}
			},

			{ -- Ft Zancudo
				Spawner = vector3(-2125.1, 2829.8, 33.0),
				InsideShop = vector3(482.7, 4811.8, -57.3),
				SpawnPoints = {
					{ coords = vector3(-2109.8, 2834.7, 32.0), heading = 354.0, radius = 3.0 },
					{ coords = vector3(-2115.4, 2834.7, 32.0), heading = 354.0, radius = 3.0 }
				}
			}
		},

		Helicopters = {
			{ -- Los Santos
				Spawner = vector3(461.1, -981.5, 43.6),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{ coords = vector3(481.2, -982.4, 41.4), heading = 90.0, radius = 10.0 }
				}
			},

			{ -- Sandy Shores
				Spawner = vector3(1730.5, 3608.0, 34.0),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{ coords = vector3(1725.1, 3618.1, 37.0), heading = 207.0, radius = 10.0 }
				}
			},

			{ -- Paleto Bay
				Spawner = vector3(-465.5, 5999.6, 31.3),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{ coords = vector3(-476.0, 5987.7, 30.8), heading = 316.0, radius = 10.0 }
				}
			}
		},

		BossActions = {
			vector3(1208.7, -1472.8, 34.0), -- El Burro Heights
			vector3(212.3, -1655.9, 29.0), -- Davis
			vector3(1692.8, 3586.5, 39.5), -- Sandy Shores
			vector3(-384.5, 6113.5, 30.4), -- Paleto Bay
			vector3(-2128.9, 2815.6, 32.0) -- Ft Zancudo
		}

	}

}

Config.AuthorizedWeapons = {
	--LSPD
	cadet = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	probationaryff = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	reserveff = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	firefighter = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	firemarshall = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	fireengineer = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	lieutenantff = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	captainff = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	battalionchief = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	deputyfirechief = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	},

	boss = {
		{ weapon = 'WEAPON_FLARE', price = 0},
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 }
	}
}

Config.AuthorizedVehicles = {
	Shared = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}
	},

	-- LSPD
	cadet = {
	},

	probationaryff = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},

	},

	reserveff = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}

	},

	firefighter = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}

	},

	firemarshall = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}

	},

	fireengineer = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}

	},

	lieutenantff = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}
	},

	captainff = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}
	},

	battalionchief = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'BCT', 		label = 'BC Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}
	},

	deputyfirechief = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'BCT', 		label = 'BC Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}
	},

	boss = {
		{model = 'ambulance3', 		label = 'Ambulance', 			price = 1},
		{model = 'IA', 		label = 'International Ambulance', 		price = 1},
		{model = 'Pump', 		label = 'Fire Truck',					price = 1},
		{model = 'BCT', 		label = 'BC Truck',					price = 1},
		{model = 'F450', 	label = 'F450 Ambulance', 				price = 1}

	}
}

Config.AuthorizedHelicopters = {
	-- LSPD
	recruit = {},

	fire1 = {},

	fire2 = {},

	sergeant = {},

	lieutenant = {},

	captain = {},

	bchief = {},

	achief = {},

	boss = {}
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Uniforms = {
	-- LSPD
	recruit_wear = {
		male = {},
		female = {}
	},
}