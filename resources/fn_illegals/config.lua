Config = {}
 
Config.MarkerColor = {r = 0, g = 200, b = 204}
Config.JobName = "police" -- required  job
Config.Bonus = 800 -- money bonus

Config.CountMoney = 500
Config.BonusMoney = 200

Config.Items = {"weed", "coke", "meth", "opium"} -- List of the illegal items (do not add weapons!)

Config.Zones = { 
    {type = 27, x = 481.87, y = -982.3, z = 23.91}, -- script zones MRPD
    {type = 27, x = 374.36, y = -1599.48, z = 28.29}, -- CSP
    {type = 27, x = 1852.7, y = 3697.88, z = 33.26}, -- SSSO
    {type = 27, x = -441.34, y = 5986.5, z = 30.72} -- PBPD
}

Config.Locale = "en" 