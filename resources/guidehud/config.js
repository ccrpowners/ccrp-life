var config = {
  'title': 'CCRP',
  'welcomeMessage': 'WELCOME TO Commercial City RP',
  // Add images for the loading screen background.
  images: [
  'https://img.skordy.com/j/RGvCc.jpeg', 'https://img.skordy.com/j/nsQy5.jpeg', 'https://img.skordy.com/j/KNlLn.jpeg',
  ],
  // Turn on/off music
  enableMusic: true,
  // Music list. (Youtube video IDs). Last one should not have a comma at the end.
  music: [
    'lS7Zx_JU-oE', 'yZfyQ0MAMmo', 'QglaLzo_aPk', 'd7JoJcEJYzY'
  ],
  // Change music volume 0-100.
  musicVolume: 01,
  // Change Discord settings and link.
  'discord': {
    'show': true,
    'discordLink': 'discord.me/commercialcityrp',
  },
  // Change which key you have set in guidehud/client/client.lua
  'menuHotkey': 'F5',
  // Turn on/off rule tabs. true/false
  'rules': {
    'generalconduct': true,
    'roleplaying': true,
    'rdmvdm': true,
    'metagaming': true,
    'newlife': true,
    'abuse': true,
  },
}

// Home page annountments.
var announcements = [
  'Read the rules, and have fun!',
  'CCRP IS CONTROLLER FRIENDLY',
  'BE SURE TO TURN OFF YOUR INGAME VOICE CHAT',
  'There have been changes made to controls within the game, Please refresh your knowledge of the controls',
  'ANNOUNCEMENT- ALL CITIZENS MUST PURCHASE A GPS FROM ANY STORE TO HAVE A MINI MAP'


]


// Add/Modify rules below.
var guidelines = [
  'Breaking any of these rules may result in administrative action.',
  'Not knowing the rules does not make players exempt from them.',
  'If another player breaks rules it does not give you the right to break a rule yourself.',
  'Trying to skirt an obvious rule may result in a ban.',
  'Staff reserves the right to ban players they feel are being toxic, disruptive or not playing by the spirit of the game mode.',
  'Not all rules can be listed so use common sense when playing.',
  'Minor rules may be superseded by excellent role-play as determined by Staff.',
]

var generalconduct = [
  'Racism, bigotry, anti-antisemitism, and any other form of harassment is not tolerated.',
  'Players can not role-play sexual assault, rape, or anything that can be deemed as intense and inappropriate behavior.',
]

var roleplaying = [
  'Players must role-play every situation.',
  '- Example: "I ran the stoplight because of server lag" or similar situations is not allowed.',
  '- Exception:  Players may only go Out Of Character when a staff member asks you to explain a situation and/or authorizes you to go OOC.',
  'Players must value their lives.',
  '- Example:  If a player has a gun to their head they must act accordingly.',
  'Players can not rule quote.',
  'Players must role-play medical injuries correctly at all times.',
  'Players can not do something intentionally in front of police that wouldn\'t ordinarily be done. This is known as "Cop Baiting".',
  'Players can not steal unattended police/medic vehicles parked at police stations or hospitals of AI, Player Vehicles are ok as long as it in RP.',
  'Players can not enter an apartment to avoid consequences or role-play.',
  'Players can not intentionally respawn, log out, or find another way to avoid or skirt potential role-play.',
  'Bluezones such as PD, Hospital, Clothing stores, Bennys and Vehicle Shops are classified as Bluezones and they are Roleplay restricted. Anyone breaking this rule will be punished',
  'Security is NOT Law Enforcement. You may only patrol in Private Property, and if patrol includes and crime, you must contact 911 to dispatch police to location.',
]

var rdmvdm = [
  'Players can not kill or attack other players without role-play.',
  'Players must have a reason or a benefit to their character when trying to kill or attack another player.',
  '- Example:  Yelling "hands up or die" without a reason is not valid role-play.',
  'Players may use vehicles as weapons as long as its within role-play and logical.',
  'Players can not intentionally use aerial vehicles to collide into other players or vehicles.',
]

var metagaming = [
  'Players can not use information gathered outside of role-play to influence their actions within the game.',
  'Players may remove another players communication devices in an role-play manner.',
  'Players with removed communication devices are expected to mute their third-party communication software.',
  'Players may only remove another players communication device when it is logical within role-play.',
  'Players can not use information gathered from outside the server (such as forums) while in-game.',
  'Knowledge and experiences should be learned and discovered by a players current character in-game.',
  'Players can not force another player into a situation that they cannot role-play out of. This is known as "Power-Gaming".',
  'Players must use common sense when encountering power-gaming potential situations.',
]

var newlife = [
  'Players that are downed but then stabilized should continue role-play accordingly.',
  'Players that are killed ("respawn" is prompted) must "forget" their previous situation in which they have died.',
  'Players that are killed may still proceed with their current character. (players may DOA their character and start a new character story).',
]

var abuse = [
  'Players can not abuse or exploit bugs.',
  'Players can not hack or script. (using third-party software, injectors, etc).',
  'Players who report an exploit using the proper procedures will be rewarded ingame.',
]

// Modify hotkeys below.
var generalhotkeys = [
  'Press your <code>TS PTT</code> to speak to players close.',
  'Press <kbd>N</kbd> to speak over radio.',
  'Press <kbd>F1</kbd> to to change current voice distance.',
  'Press <kbd>COMMA</kbd> to view player list.',
  'Press <kbd>LeftShift</kbd> + <kbd>~</kbd> to open your personal inventory.',
  'Press <kbd>M</kbd> to open vMenu.',
  'Press <kbd>T</kbd> to open your chat.',
  'Press <kbd>F7</kbd> to open your invoices.',
  'Press <kbd>P</kbd> to open your the map.',
  'Type <code>/mcard</code> for Miranda Rights card. (Police)',
  'Type <code>/vcard</code> for Vital Signs card (EMS & Police)',
  'Type <code>/report</code> report a player',
  'Press <kbd>[</kbd> to open Documents.',
]

var rphotkeys = [
  'Use <code>/911</code> in chat to call 911.',
  'Press <kbd>X</kbd> to put your hands up.',
  'Press <kbd>Ins</kbd> to bring up your phone.',
  'Press <kbd>LEFT CTRL</kbd> to crouch.',
  'Press <kbd>B</kbd> to point.',
  'Press <kbd>R</kbd> to ragdoll.',
  'Type <code>/carry</code> to carry a person.',
  'Type <code>/huk</code> hands up on knees.',
  'Type <code>/postal [postal number]</code> draws route to postal.',
  'Type <code>/vape start/stop</code> to start vaping.',
]

var vehiclehotkeys = [
  'Press <kbd>K</kbd> to put your seatbelt on.',
  'Press <kbd>Y</kbd> to open your glovebox.',
  'Press <kbd>LEFT SHIFT</kbd> + <kbd>Y</kbd> to open your vehicles inventory, Outside of Vehicle.',
  'Press <kbd>F9</kbd> to turn on/off Cruise Control.',
  'Type <code>/seat</code> to turn on/off seat shuffle.',
  'Press <kbd>Q</kbd> to turn on emergency lights. (EMS & Police)',
  'Press <kbd>LEFT ALT</kbd> to turn on sirens. (EMS & Police)',
  'Press <kbd>R</kbd> to change siren. (EMS & Police)',
]

var jobshotkeys = [
  'Press <kbd>F6</kbd> to bring up your job menu.',
  'Press <kbd>F5</kbd> to bring up vehicle radar. (Police)',
  'Press <kbd>~</kbd> to turn on keybind lock for radar. (Police)',
  'Type <code>/tc</code> to bring up Traffic Control (Police)',
  'Type <code>/setspike</code> set spikes (police).',
  'Type <code>/delspike</code> deletes spikes (Police).',
  'Type <kbd>LeftShift</kbd> + <kbd>G</kbd> to tackle a suspect (Police).',
  'Type <code>/spawnstr</code> spawn strecher (EMS).',
  'Type <code>/pushstr</code> push stretcher (EMS).',
  'Type <code>/getintostr</code> get into stretcher (Civ).',
  'Type <code>/togglestr</code> put in pull out stretcher (EMS).',
  'Type <code>/openbaydoors</code> open back doors of the ambulance (EMS).',
  'Press <kbd>x</kbd> stop pushing strecher/get out of stretcher (EMS, Civ).',
  'Press <kbd>LeftShift</kbd> to blip AI vehicle (Police).',
]