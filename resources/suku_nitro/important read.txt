IMPORTANT!
!!!![Before you launch the server after installing suku_nitro IMPORT the .sql into your database otherwise it will not work]!!!!

If you are using Inventoryhud and would like an image for the nitro, follow this simple instruction:

- Go to the esx_inventoryhud folder in your resources
- open the __resource.lua
- Scroll down to files {
- Insert this line before the closing } of files: 
	"html/img/items/nitrocannister.png", 
	"html/img/items/wrench.png",

- Save and close
- browse to html/img/items in the esx_inventoryhud resource folder and add both .pngs into the folder.

https://youtu.be/P0C_3k7SFyw